package com.chenfy.touchtransfer_android.base;


import androidx.recyclerview.widget.RecyclerView;

import com.chenfy.touchtransfer_android.dao.FileEntity;
import com.chenfy.touchtransfer_android.ui.adapter.GalleryAdapter;

import java.util.List;

/**
 * Created by ChenFengyao
 * Date: 20-11-9
 */
public abstract class BaseRecyclerViewAdapter<VH extends BaseViewHolder, Data> extends RecyclerView.Adapter<VH> {
    protected List<Data> mData;

    @Override
    public int getItemCount() {
        return mData == null ? 0 : mData.size();
    }

    // 向结尾添加数据
    public void addFileEntity(Data data) {
        mData.add(data);
        notifyItemInserted(mData.size() - 1);
    }

    // 设置全部数据
    public BaseRecyclerViewAdapter setData(List<Data> data) {
        mData = data;
        notifyDataSetChanged();
        return this;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    // 清除所有
    public void clear() {
        int size = mData.size();
        mData.clear();
        notifyItemRangeRemoved(0, size);
    }

    // 获取指定位置Data
    public Data getData(int pos) {
        return mData == null ? null : mData.get(pos);
    }
}
