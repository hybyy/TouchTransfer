package com.chenfy.touchtransfer_android.base;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.IdRes;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by ChenFengyao
 * Date: 20-9-4
 */
public abstract class BaseViewHolder extends RecyclerView.ViewHolder {

    public BaseViewHolder(@NonNull ViewGroup parent, @LayoutRes int layoutId) {
        this(LayoutInflater.from(parent.getContext())
                .inflate(layoutId, parent, false));
    }

    public BaseViewHolder(@NonNull View itemView) {
        super(itemView);
        bindViews(itemView);
    }

    protected abstract void bindViews(View itemView);

    public Context getContext() {
        return itemView.getContext();
    }
}
