package com.chenfy.touchtransfer_android.ui;

import android.app.SharedElementCallback;
import android.content.Intent;
import android.os.Bundle;
import android.transition.Fade;
import android.util.Log;
import android.view.View;
import android.view.Window;

import androidx.annotation.Nullable;
import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.chenfy.touchtransfer_android.R;
import com.chenfy.touchtransfer_android.base.BaseActivity;
import com.chenfy.touchtransfer_android.base.bindview.BindLayout;
import com.chenfy.touchtransfer_android.base.bindview.BindView;
import com.chenfy.touchtransfer_android.dao.FileEntity;
import com.chenfy.touchtransfer_android.filehandle.FileManager;
import com.chenfy.touchtransfer_android.ui.adapter.GalleryAdapter;
import com.chenfy.touchtransfer_android.view.OnRecyclerItemClickListener;

import java.util.List;
import java.util.Map;

import static com.chenfy.touchtransfer_android.dao.Contact.ANIM.IMG;
import static com.chenfy.touchtransfer_android.util.TransitionUtil.buildShareElementsTransition;

/**
 * Created by ChenFengyao
 * Date: 20-9-3
 */
@BindLayout(R.layout.activity_gallery)
public class GalleryActivity extends BaseActivity {

    @BindView(R.id.gallery_rv)
    private RecyclerView mGalleryRv;


    private GalleryAdapter mGalleryAdapter;
    private List<FileEntity> mAll;

    @Override
    public void initWindow(Window window) {
        super.initWindow(window);
        // 改变状态栏颜色
//        window.setStatusBarColor(getColor(R.color.status_color));
//        window.setNavigationBarColor(getColor(R.color.native_bar_color));
//        window.getDecorView().setSystemUiVisibility(
//                View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
//                        | View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR);

        // 设置WindowTransition,除指定的ShareElement外，其它所有View都会执行这个Transition动画
        window.setEnterTransition(new Fade());
        window.setExitTransition(new Fade());

        // 设置ShareElementTransition,指定的ShareElement会执行这个Transition动画
        window.setSharedElementEnterTransition(buildShareElementsTransition());

    }


    @Override
    public void initViews() {
        super.initViews();

        mGalleryAdapter = new GalleryAdapter();
        LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false);
        mGalleryRv.setLayoutManager(mLinearLayoutManager);
        PagerSnapHelper snapHelper = new PagerSnapHelper();
        snapHelper.attachToRecyclerView(mGalleryRv);
        mGalleryRv.setAdapter(mGalleryAdapter);

    }

    @Override
    protected void initData(@Nullable Bundle savedInstanceState, Intent preIntent) {
        super.initData(savedInstanceState, preIntent);
        int pos = preIntent.getIntExtra(KEY_POS, 0);

        mAll = FileManager.getInstance().getAll();
        mGalleryAdapter.setData(mAll);
        mGalleryRv.scrollToPosition(pos);

        // 设置动画ID
        ViewCompat.setTransitionName(mGalleryRv, IMG + pos);

    }


    @Override
    public void onBackPressed() {
        View childAt = mGalleryRv.getLayoutManager().getChildAt(0);
        int position = mGalleryRv.getLayoutManager().getPosition(childAt);
        setResult(RESULT_OK, new Intent().putExtra("pos", position));
        supportFinishAfterTransition();

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void initListeners() {
        mGalleryRv.addOnItemTouchListener(new OnRecyclerItemClickListener(mGalleryRv) {
            @Override
            public void onItemClick(RecyclerView.ViewHolder vh, int childId) {
                if (childId == R.id.item_play) {
                    int pos = vh.getAdapterPosition();
                    FileEntity data = mGalleryAdapter.getData(pos);
                    String rawPath = data.getRawPath();
                    Intent intent = new Intent(GalleryActivity.this, VideoAty.class);
                    intent.putExtra(KEY_VIDEO_PATH, rawPath);
                    startActivity(intent);
                }
            }
        });


        // 设置动画的监听
        setEnterSharedElementCallback(new SharedElementCallback() {
            @Override
            public void onMapSharedElements(List<String> names, Map<String, View> sharedElements) {

                sharedElements.clear();// 清除之前的共享元素
                names.clear();

                View childAt = mGalleryRv.getLayoutManager().getChildAt(0);
                if (childAt != null) {

                    View viewById = childAt.findViewById(R.id.item_gallery_iv);
                    int position = mGalleryRv.getLayoutManager().getPosition(childAt);
                    Log.d("GalleryActivity", "position:" + position);

                    names.add(IMG + position);
                    sharedElements.put(IMG + position, viewById);
                }
            }
        });
    }

}
