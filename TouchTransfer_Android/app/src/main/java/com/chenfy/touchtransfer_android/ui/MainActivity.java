package com.chenfy.touchtransfer_android.ui;

import android.app.SharedElementCallback;
import android.content.Intent;
import android.database.ContentObserver;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.text.TextUtils;
import android.transition.TransitionSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chenfy.touchtransfer_android.ui.adapter.AlbumAdapter;
import com.chenfy.touchtransfer_android.dao.FileEntity;
import com.chenfy.touchtransfer_android.R;
import com.chenfy.touchtransfer_android.base.BaseActivity;
import com.chenfy.touchtransfer_android.base.bindview.BindLayout;
import com.chenfy.touchtransfer_android.base.bindview.BindView;
import com.chenfy.touchtransfer_android.filehandle.FileManager;
import com.chenfy.touchtransfer_android.network.NetWorkManager;
import com.chenfy.touchtransfer_android.util.ActivityUtil;
import com.chenfy.touchtransfer_android.util.DialogUtil;
import com.chenfy.touchtransfer_android.util.VibratorUtil;
import com.chenfy.touchtransfer_android.view.EmptyRecyclerView;
import com.chenfy.touchtransfer_android.view.MarginDecoration;
import com.chenfy.touchtransfer_android.view.OnRecyclerItemClickListener;
import com.google.zxing.integration.android.IntentIntegrator;

import java.util.List;
import java.util.Map;

import static com.chenfy.touchtransfer_android.dao.Contact.ANIM.IMG;
import static com.chenfy.touchtransfer_android.dao.Contact.DB.PROVIDER_DEL_URI_STR;
import static com.chenfy.touchtransfer_android.dao.Contact.DB.PROVIDER_INSERT_URI_STR;
import static com.chenfy.touchtransfer_android.dao.Contact.DIALOG_TYPE.TYPE_EXIT;
import static com.chenfy.touchtransfer_android.dao.Contact.THUMB_COLUMN_NUM;
import static com.chenfy.touchtransfer_android.util.TransitionUtil.buildDefaultTransition;
import static com.chenfy.touchtransfer_android.util.TransitionUtil.buildShareElementsTransition;

@BindLayout(R.layout.activity_main)
public class MainActivity extends BaseActivity {

    @BindView(R.id.album_rv)
    private EmptyRecyclerView mAlbumRv;
    @BindView(value = R.id.main_tool_bar)
    private Toolbar mToolbar;
    @BindView(value = R.id.add_iv, click = true)
    private View mAddView;
    @BindView(R.id.main_empty_view)
    private View mEmptyView;

    private AlbumAdapter mAlbumAdapter;
    private ContentObserver mInsertObservable;
    private ContentObserver mDelObservable;
    private NetWorkManager mManager;
    private int mBackPos = -1; // 用来记录从大图页面返回的索引信息
    private GridLayoutManager mLayoutManager;

    @Override
    public void initWindow(Window window) {
        super.initWindow(window);

        // 除了共享元素外,其他View的退出方式
        window.setExitTransition(buildDefaultTransition()); // 透明度

        TransitionSet transitionSet = buildShareElementsTransition();
        window.setSharedElementExitTransition(transitionSet);
    }

    @Override
    public void initViews() {
        super.initViews();

        ActivityUtil.enableAty(NFCActivity.class);

        setSupportActionBar(mToolbar);
        mAlbumRv.setEmptyView(mEmptyView);
        mAlbumAdapter = new AlbumAdapter();
        mLayoutManager = new GridLayoutManager(this, THUMB_COLUMN_NUM);
        mAlbumRv.setLayoutManager(mLayoutManager);
        mAlbumRv.setAdapter(mAlbumAdapter);

        mAlbumRv.addItemDecoration(new MarginDecoration(10));//10像素的间隔
    }

    @Override
    protected void initData(@Nullable Bundle savedInstanceState, Intent preIntent) {
        super.initData(savedInstanceState, preIntent);
        List<FileEntity> all = FileManager.getInstance().getAll();
        mManager = new NetWorkManager();
        mAlbumAdapter.addFileEntities(all);
        Handler handler = new Handler();
        mInsertObservable = new ContentObserver(handler) {
            @Override
            public void onChange(boolean selfChange, Uri uri) {
                String fileID = uri.getLastPathSegment();
                FileEntity fileEntity = FileManager.getInstance().getByFileID(fileID);
                mAlbumAdapter.addFileEntity(fileEntity);
            }
        };
        getContentResolver().registerContentObserver(Uri.parse(PROVIDER_INSERT_URI_STR),
                true, mInsertObservable);

        mDelObservable = new ContentObserver(handler) {
            @Override
            public void onChange(boolean selfChange, Uri uri) {
                String delID = uri.getLastPathSegment();
                if ("all".equals(delID)) {
                    mAlbumAdapter.clear();
                }
            }
        };
        getContentResolver().registerContentObserver(Uri.parse(PROVIDER_DEL_URI_STR),
                true, mDelObservable);


    }

    @Override
    public void initListeners() {
        super.initListeners();
        mAlbumRv.addOnItemTouchListener(new OnRecyclerItemClickListener(mAlbumRv) {
            @Override
            public void onItemClick(RecyclerView.ViewHolder vh, int childId) {
                int pos = vh.getAdapterPosition();
                Bundle translateBundle = null; // 设置动画效果
                if (vh instanceof AlbumAdapter.AlbumVH) {

                    ActivityOptionsCompat options = ActivityOptionsCompat
                            .makeSceneTransitionAnimation(MainActivity.this, ((AlbumAdapter.AlbumVH) vh).mAlbumIv,
                                    String.valueOf(ViewCompat.getTransitionName(((AlbumAdapter.AlbumVH) vh).mAlbumIv)));

                    translateBundle = options.toBundle();
                }
                if (translateBundle == null) {
                    translateBundle = ActivityOptionsCompat.makeCustomAnimation(MainActivity.this, R.anim.galery_in,
                            0).toBundle();
                }

                showGallery(pos, translateBundle);
            }
        });


        // 设置动画回调
        setExitSharedElementCallback(new SharedElementCallback() {
            // 用于动画开始前替换ShareElements
            @Override
            public void onMapSharedElements(List<String> names, Map<String, View> sharedElements) {
                super.onMapSharedElements(names, sharedElements);
                Log.d("DH---B", "names:" + names);
                if (mBackPos >= 0) {
                    // 当从大图页面返回时, 需要重新设定共享元素的id
                    Log.d("MainActivity---", "in");
                    names.clear();
                    sharedElements.clear();
                    names.add(IMG + mBackPos);
                    View viewByPosition = mLayoutManager.findViewByPosition(mBackPos);
                    if (viewByPosition != null) {
                        View albumIv = viewByPosition.findViewById(R.id.item_album_iv);
                        sharedElements.put(IMG + mBackPos,
                                albumIv);
                    }
                    mBackPos = -1;
                }
            }

            /**
             *在这里会把ShareElement里值得记录的信息存到为Parcelable格式，以发送到Activity B
             *默认处理规则是ImageView会特殊记录Bitmap、ScaleType、Matrix，其它View只记录大小和位置
             */
            @Override
            public Parcelable onCaptureSharedElementSnapshot(View sharedElement, Matrix viewToGlobalMatrix, RectF screenBounds) {
                return super.onCaptureSharedElementSnapshot(sharedElement, viewToGlobalMatrix, screenBounds);
            }
        });

        // 设置放大缩小的监听
        mAlbumRv.setOnScaleListener(new EmptyRecyclerView.OnScaleListener() {
            @Override
            public void onZoomIn() {
                // 放大
                THUMB_COLUMN_NUM = Math.max(1, --THUMB_COLUMN_NUM);

                mLayoutManager.setSpanCount(THUMB_COLUMN_NUM);
                mAlbumAdapter.notifyItemRangeChanged(THUMB_COLUMN_NUM, mAlbumAdapter.getItemCount() - THUMB_COLUMN_NUM);

            }

            @Override
            public void onZoomOut() {
                mLayoutManager.setSpanCount(++THUMB_COLUMN_NUM);
                mAlbumAdapter.notifyItemRangeChanged(THUMB_COLUMN_NUM - 1,
                        mAlbumAdapter.getItemCount() - THUMB_COLUMN_NUM + 1);
            }
        });
    }

    private void showGallery(int pos, Bundle bundle) {

        Intent intent = new Intent(this, GalleryActivity.class);
        intent.putExtra(KEY_POS, pos);

        //添加过度的动画效果
        startActivityForResult(intent, 1, bundle);
    }

    @Override
    protected void onDestroy() {
        ActivityUtil.disableAty(NFCActivity.class);
        getContentResolver().unregisterContentObserver(mInsertObservable);
        getContentResolver().unregisterContentObserver(mDelObservable);
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_bind_nfc:
                ActivityUtil.enableAty(NFCWriteActivity.class);
                getQRIntent()
                        .initiateScan();// 初始化扫码
                break;
            case R.id.action_scan_qr:
                getQRIntent()
                        .addExtra(KEY_QR, true)
                        .initiateScan();
                break;
            case R.id.action_del_all:
                // 清除所有
                FileManager.getInstance().deleteAll();
                break;
            case R.id.action_about:
                // 关于页面
                startActivity(AboutActivity.class);
                break;
            case R.id.action_help:
                startActivity(HelpActivity.class);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private IntentIntegrator getQRIntent() {
        return new IntentIntegrator(this)
                // 自定义Activity，重点是这行----------------------------
                .setCaptureActivity(ScanQRActivity.class)
                .setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES)// 扫码的类型,可选：一维码，二维码，一/二维码
                .setPrompt("请扫描PC端二维码")// 设置提示语
                .setCameraId(0)// 选择摄像头,可使用前置或者后置
                .setBeepEnabled(false)// 是否开启声音,扫完码之后会"哔"的一声
                .setBarcodeImageEnabled(false);// 扫完码之后生成二维码的图片
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        String nfcId = intent.getStringExtra(KEY_NFC_ID);
        if (!TextUtils.isEmpty(nfcId)
                && mAlbumAdapter.getItemCount() > 0
                && !mManager.isUploading()) {
            VibratorUtil.vibrator();
            mManager.transAllFile(nfcId, new NetWorkManager.TransListener() {

                @Override
                public void onInit() {
                    ActivityUtil.disableAty(NFCActivity.class);
                    mAlbumAdapter.startUpload();
                    Toast.makeText(MainActivity.this, "准备传输", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onTransferFinish(String fileID) {
                    Log.d("MainActivity", "传输完成:" + fileID);
                    mAlbumAdapter.uploadFinish(fileID);
                }

                @Override
                public void onTransferFinishAll() {
                    ActivityUtil.enableAty(NFCActivity.class);
                    VibratorUtil.vibrator();
                    Toast.makeText(MainActivity.this, "传输完成", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onConnFailed() {
                    ActivityUtil.enableAty(NFCActivity.class);
                    Toast.makeText(MainActivity.this, "没有找到PC", Toast.LENGTH_SHORT).show();
                    mAlbumAdapter.resetAll();
                }
            });
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && mAlbumAdapter.getItemCount() > 0) {
            showExitDialog();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void showExitDialog() {
        DialogUtil.getDialog(this, TYPE_EXIT, () -> {
            FileManager.getInstance().deleteAll();
            finish();
        }, null).show();
    }

    @Override
    public void doClick(View v) {
        switch (v.getId()) {
            case R.id.add_iv:
                FileManager.getInstance().openFileChooser(this);
                break;
        }
    }

    @Override
    public void onActivityReenter(int resultCode, Intent data) {
        super.onActivityReenter(resultCode, data);
        /*
        1. 先暂停动画
        2. 设置RecyclerView的绘制监听
        3. 让Recycler滚动到指定位置
        4. 如果RecyclerView 绘制完成了,再开始动画效果
         */
        supportPostponeEnterTransition(); // 暂停动画
        mAlbumRv.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                mAlbumRv.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                // 重新开始动画
                supportStartPostponedEnterTransition();
            }
        });

        mBackPos = data.getIntExtra("pos", 0);
        Log.d("MainActivity", "mBackPos:" + mBackPos);
        // 滚动到指定位置
        mAlbumRv.scrollToPosition(mBackPos);
    }
}