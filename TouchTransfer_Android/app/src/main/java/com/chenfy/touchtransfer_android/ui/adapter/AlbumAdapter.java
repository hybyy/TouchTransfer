package com.chenfy.touchtransfer_android.ui.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.FitCenter;
import com.chenfy.touchtransfer_android.base.BaseRecyclerViewAdapter;
import com.chenfy.touchtransfer_android.base.BaseViewHolder;
import com.chenfy.touchtransfer_android.dao.FileEntity;
import com.chenfy.touchtransfer_android.R;

import java.util.ArrayList;
import java.util.List;

import static com.chenfy.touchtransfer_android.dao.Contact.ANIM.IMG;

public class AlbumAdapter extends BaseRecyclerViewAdapter<AlbumAdapter.AlbumVH,FileEntity> {

    public AlbumAdapter() {
        mData = new ArrayList<>();
    }

    public void startUpload() {
        for (FileEntity fileEntity : mData) {
            fileEntity.setUploading(true);
        }
        notifyDataSetChanged();
    }

    public void uploadFinish(String fileID) {
        for (int i = 0; i < mData.size(); i++) {
            FileEntity fileEntity = mData.get(i);
            if (fileEntity.getFileID().equals(fileID)) {
                fileEntity.setUploading(false);
                notifyItemChanged(i);
                return;
            }
        }
    }

    public AlbumAdapter addFileEntities(List<FileEntity> fileEntities) {
        mData.addAll(fileEntities);
        notifyDataSetChanged();
        return this;
    }

    @NonNull
    @Override
    public AlbumVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new AlbumVH(parent, R.layout.item_album);
    }

    @Override
    public void onBindViewHolder(@NonNull AlbumVH holder, int position) {
        FileEntity fileEntity = mData.get(position);
        Glide.with(holder.getContext())
                .load(fileEntity.getRawPath())
                .transform(new FitCenter())
                .into(holder.mAlbumIv);
        if (fileEntity.isImg()) {
            holder.mVideoTypeIv.setVisibility(View.GONE);
            holder.mDurationBar.setVisibility(View.GONE);
        } else {
            holder.mVideoTypeIv.setVisibility(View.VISIBLE);
            holder.mDurationBar.setVisibility(View.VISIBLE);
            holder.mDurationTv.setText(fileEntity.getVideoDuration());
        }
        if (fileEntity.isUploading()) {
            holder.mProgressBar.setVisibility(View.VISIBLE);
            holder.mBg.setVisibility(View.VISIBLE);
        } else {
            holder.mProgressBar.setVisibility(View.GONE);
            holder.mBg.setVisibility(View.GONE);
        }
        // 设置动画参数
        ViewCompat.setTransitionName(holder.mAlbumIv, IMG + position);
    }

    public void resetAll() {
        for (FileEntity fileEntity : mData) {
            fileEntity.setUploading(false);
        }
        notifyDataSetChanged();
    }

    public static class AlbumVH extends BaseViewHolder {
        public ImageView mAlbumIv;
        public ImageView mVideoTypeIv;
        public ProgressBar mProgressBar;
        public View mBg;
        public View mDurationBar;
        public TextView mDurationTv;

        public AlbumVH(@NonNull ViewGroup parent, int layoutId) {
            super(parent, layoutId);
        }

        @Override
        protected void bindViews(View itemView) {
            mAlbumIv = itemView.findViewById(R.id.item_album_iv);
            mVideoTypeIv = itemView.findViewById(R.id.item_video_type_iv);
            mProgressBar = itemView.findViewById(R.id.item_progress);
            mBg = itemView.findViewById(R.id.item_progress_bg);
            mDurationBar = itemView.findViewById(R.id.item_video_duration_info_bar);
            mDurationTv = itemView.findViewById(R.id.item_video_duration_tv);
        }
    }

}
