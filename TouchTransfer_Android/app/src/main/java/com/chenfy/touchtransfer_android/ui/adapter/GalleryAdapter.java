package com.chenfy.touchtransfer_android.ui.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.chenfy.touchtransfer_android.R;
import com.chenfy.touchtransfer_android.base.BaseRecyclerViewAdapter;
import com.chenfy.touchtransfer_android.base.BaseViewHolder;
import com.chenfy.touchtransfer_android.dao.FileEntity;

import java.util.List;

/**
 * Created by ChenFengyao
 * Date: 20-9-3
 */
public class GalleryAdapter extends BaseRecyclerViewAdapter<BaseViewHolder,FileEntity> {
    private static final String TAG = "GalleryAdapter";
    private static final int TYPE_IMG = 0;
    private static final int TYPE_VIDEO = 1;


    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == TYPE_IMG) {
            return new ImgVH(parent, R.layout.item_gallery);
        } else {
            return new VideoVH(parent, R.layout.item_gallery_video);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        ImageView target = getItemViewType(position) == TYPE_IMG ?
                ((ImgVH) holder).mImageView :
                ((VideoVH) holder).mPreImg;
        String rawPath = mData.get(position).getRawPath();
        Glide.with(holder.getContext())
                .load(rawPath)
                .into(target);
    }

    @Override
    public int getItemViewType(int position) {
        FileEntity fileEntity = mData.get(position);
        if (fileEntity.isImg()) {
            return TYPE_IMG;
        }
        return TYPE_VIDEO;
    }

    static class ImgVH extends BaseViewHolder {
        private ImageView mImageView;

        public ImgVH(@NonNull ViewGroup parent, int layoutId) {
            super(parent, layoutId);
        }

        @Override
        protected void bindViews(View itemView) {
            mImageView = itemView.findViewById(R.id.item_gallery_iv);
        }

    }


    static class VideoVH extends BaseViewHolder {
        private ImageView mPreImg;

        public VideoVH(@NonNull ViewGroup parent, int layoutId) {
            super(parent, layoutId);
        }

        @Override
        protected void bindViews(View itemView) {
            mPreImg = itemView.findViewById(R.id.item_gallery_iv);
        }

    }
}

