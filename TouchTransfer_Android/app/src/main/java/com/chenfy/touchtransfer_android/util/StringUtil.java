package com.chenfy.touchtransfer_android.util;

/**
 * Created by ChenFengyao
 * Date: 20-11-4
 */
public class StringUtil {
    private static final int SECOND = 1000;
    private static final int MINUTE = SECOND * 60;

    public static String formatTime(int timeMS) {
        StringBuilder result = new StringBuilder();
        int minute = timeMS / MINUTE;
        if (minute < 10) {
            result.append(0);
        }
        result.append(minute)
                .append(":");
        int second = Math.round(timeMS % MINUTE * 1f / SECOND);
        if (second < 10) {
            result.append(0);
        }
        result.append(second);
        return result.toString();
    }
}
