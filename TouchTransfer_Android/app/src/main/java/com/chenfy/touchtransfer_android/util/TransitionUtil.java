package com.chenfy.touchtransfer_android.util;

import android.transition.ChangeBounds;
import android.transition.ChangeClipBounds;
import android.transition.ChangeImageTransform;
import android.transition.ChangeTransform;
import android.transition.Explode;
import android.transition.Fade;
import android.transition.TransitionSet;

import static com.chenfy.touchtransfer_android.dao.Contact.ANIM.DEFAULT_ANIM_DURATION;

/**
 * Created by ChenFengyao
 * Date: 20-11-6
 */
public class TransitionUtil {
    /**
     * 创建共享动画的动画方式
     * @return 动画合集
     */
    public static TransitionSet buildShareElementsTransition() {
        TransitionSet transitionSet = new TransitionSet();
        transitionSet.addTransition(new ChangeClipBounds());
        transitionSet.addTransition(new ChangeTransform());
        transitionSet.addTransition(new ChangeBounds());
        transitionSet.addTransition(new ChangeImageTransform());
        transitionSet.setDuration(DEFAULT_ANIM_DURATION);
        return transitionSet;
    }

    /**
     * 设置默认的非共享元素动画
     * @return 动画合集
     */
    public static TransitionSet buildDefaultTransition(){
        TransitionSet transitionSet = new TransitionSet();
        transitionSet.addTransition(new Fade()); // 透明度动画
        transitionSet.setDuration(DEFAULT_ANIM_DURATION);
        return transitionSet;
    }
}
