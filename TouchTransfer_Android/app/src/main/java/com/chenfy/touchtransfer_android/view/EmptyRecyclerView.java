package com.chenfy.touchtransfer_android.view;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

public class EmptyRecyclerView extends RecyclerView {
    private View emptyView;
    private ScaleGestureDetector mScaleGestureDetector;
    private OnScaleListener mOnScaleListener;

    public EmptyRecyclerView setOnScaleListener(OnScaleListener onScaleListener) {
        mOnScaleListener = onScaleListener;
        return this;
    }

    final private AdapterDataObserver observer = new AdapterDataObserver() {
        @Override
        public void onChanged() {
            checkIfEmpty();
        }

        @Override
        public void onItemRangeInserted(int positionStart, int itemCount) {
            checkIfEmpty();
        }

        @Override
        public void onItemRangeRemoved(int positionStart, int itemCount) {
            checkIfEmpty();
        }
    };
    private ScaleGestureDetector.OnScaleGestureListener mListener = new ScaleGestureDetector.OnScaleGestureListener() {
        private float result;
        @Override
        public boolean onScale(ScaleGestureDetector detector) {
//            Log.d("EmptyRecyclerView", "detector.getScaleFactor()0:" + detector.getScaleFactor());
            float scaleFactor = detector.getScaleFactor();
            if (Float.isNaN(scaleFactor) || Float.isInfinite(scaleFactor))
                return false;
            result += scaleFactor - 1f;
            return true;
        }

        @Override
        public boolean onScaleBegin(ScaleGestureDetector detector) {
            Log.d("EmptyRecyclerView", ">>>>>>");
            result = 0;
            return true;
        }

        @Override
        public void onScaleEnd(ScaleGestureDetector detector) {
            Log.d("EmptyRecyclerView", "<<<<<<<<<<<<<" + result);

            if (mOnScaleListener == null) {
                return;
            }
            if (result >= 0.4f){
                // 放大
                mOnScaleListener.onZoomIn();;

            }else if (result <= -0.4f){
                // 缩小
                mOnScaleListener.onZoomOut();
            }

        }
    };

    public EmptyRecyclerView(Context context) {
        super(context);
        mScaleGestureDetector = new ScaleGestureDetector(context, mListener);
    }

    public EmptyRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mScaleGestureDetector = new ScaleGestureDetector(context, mListener);
    }

    public EmptyRecyclerView(Context context, AttributeSet attrs,
                             int defStyle) {
        super(context, attrs, defStyle);
        mScaleGestureDetector = new ScaleGestureDetector(context, mListener);
    }

    private void checkIfEmpty() {
        if (emptyView != null && getAdapter() != null) {
            final boolean emptyViewVisible = getAdapter().getItemCount() == 0;
            emptyView.setVisibility(emptyViewVisible ? VISIBLE : GONE);
            setVisibility(emptyViewVisible ? GONE : VISIBLE);
        }
    }

    @Override
    public void setAdapter(Adapter adapter) {
        final Adapter oldAdapter = getAdapter();
        if (oldAdapter != null) {
            oldAdapter.unregisterAdapterDataObserver(observer);
        }
        super.setAdapter(adapter);
        if (adapter != null) {
            adapter.registerAdapterDataObserver(observer);
        }
        checkIfEmpty();
    }

    public void setEmptyView(View view) {
        this.emptyView = view;
        checkIfEmpty();
    }

    @Override
    public boolean onTouchEvent(MotionEvent e) {
        if (e.getPointerCount() > 1){
            mScaleGestureDetector.onTouchEvent(e);
            return true;
        }
        return super.onTouchEvent(e);
    }

    @Override
    public boolean onInterceptHoverEvent(MotionEvent event) {
        if (event.getPointerCount() > 1) {
            return true; // 拦截多指手势
        }
        return super.onInterceptHoverEvent(event);
    }

    public interface OnScaleListener {
        /**
         * 放大
         */
        void onZoomIn();

        /**
         * 缩小
         */
        void onZoomOut();
    }

}
