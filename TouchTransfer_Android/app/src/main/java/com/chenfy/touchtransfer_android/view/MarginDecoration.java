package com.chenfy.touchtransfer_android.view;

import android.graphics.Rect;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

import static com.chenfy.touchtransfer_android.dao.Contact.THUMB_COLUMN_NUM;

public class MarginDecoration extends RecyclerView.ItemDecoration {

    private int margin;

    public MarginDecoration(int margin) {
        this.margin = margin;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        int itemPosition = parent.getChildLayoutPosition(view);
        int l = 0, t = 0, r = 0, b = 0;
        if (itemPosition >= THUMB_COLUMN_NUM) {
            t = margin;  //除了第一行, 每一行距离上面都有 margin的距离
        }
        l = r = margin / 2;
        parent.setPadding(margin / 2,
                parent.getPaddingTop(),
                margin / 2,
                parent.getPaddingBottom());

        outRect.set(l, t, r, b);

    }

}
